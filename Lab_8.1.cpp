﻿/*Ввести последовательность натуральных чисел {Aj}j=1...n (n<=1000). Упорядочить последовательность по неубыванию 
первой цифры числа, числа с одинаковыми первыми цифрами дополнительно упорядочить по неубыванию суммы цифр числа, 
числа с одинаковыми первыми цифрами и одинаковыми суммами цифр дополнительно упорядочить по неубыванию самого числа.

Переделайте задание #2 из лабораторной работы #4 своего варианта, разместив массивы в динамической
памяти. Размеры массивов задайте согласно введенным данным.*/

#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");

    int N;

    cout << "Введите колличество символов до 1000: " << endl;
    cin >> N;

    if (N < 1 || N > 1000)
    {
        cout << "Количество символов должно быть натуральным числом и не превышать 1000!";
        return 2;
    }

    int* mas = new int[N];
    int* First = new int[N];
    int* Sum = new int[N];

    cout << "\n" << "Введите натуральные числа через Enter." << endl;

    for (int i = 0; i < N; i++)
    {
        cin >> mas[i];
        if (mas[i] < 1)
        {
            cout << "Введено не натуральное число!";
            return 1;
        }

        int examination = mas[i];
        int reverse = 0;

        while (examination > 0)
        {
            reverse = reverse * 10 + examination % 10;
            examination = examination / 10;
            if (examination == 0)
                break;
        }

        First[i] = reverse % 10;

        Sum[i] = 0;
        int x = mas[i];
        while (x > 0)
        {
            Sum[i] += x % 10;
            x /= 10;
        }
    }

    for (int i = 0; i < N - 1; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
            if ((First[i] > First[j]) ||
                (First[i] == First[j] && Sum[i] > Sum[j]) ||
                (First[i] == First[j] && Sum[i] == Sum[j] && mas[i] > mas[j]))
            {
                swap(mas[i], mas[j]);
                swap(First[i], First[j]);
                swap(Sum[i], Sum[j]);
            }
        }
    }

    cout << "\n" << "Упорядоченный массив готов!" << endl;

    for (int i = 0; i < N; i++)
        cout << mas[i] << "  ";

    return 0;
}